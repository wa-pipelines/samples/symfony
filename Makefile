current-dir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
DATE := $(shell date +%Y)
SHELL = /bin/bash

ifneq (,$(findstring xterm,${TERM}))
	BLACK        := $(shell tput -Txterm setaf 0)
	RED          := $(shell tput -Txterm setaf 1)
	GREEN        := $(shell tput -Txterm setaf 2)
	YELLOW       := $(shell tput -Txterm setaf 3)
	LIGHTPURPLE  := $(shell tput -Txterm setaf 4)
	PURPLE       := $(shell tput -Txterm setaf 5)
	BLUE         := $(shell tput -Txterm setaf 6)
	WHITE        := $(shell tput -Txterm setaf 7)
	RESET        := $(shell tput -Txterm sgr0)
else
	BLACK        := ""
	RED          := ""
	GREEN        := ""
	YELLOW       := ""
	LIGHTPURPLE  := ""
	PURPLE       := ""
	BLUE         := ""
	WHITE        := ""
	RESET        := ""
endif

PROJECT="symfony"

default: help

##@ Helpers
.PHONY: help
help: ## Display this help
	@awk 'BEGIN {FS = ":.*##"; printf "\n\033[1;34m${PROJECT}\033[0m\tCopyright (c) ${DATE} Symfony Demo\n \nUsage:\n  make \033[1;34m<target>\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[1;34m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

##@ Building
.PHONY: bootstrap
bootstrap: ## is used solely for fulfilling dependencies of the project
	@./scripts.d/boostrap.sh

.PHONY: setup
setup: bootstrap ## is used to set up a project in an initial state
	@./scripts.d/setup.sh

.PHONY: update
update: ## is used to update the project after a fresh pull
	@./scripts.d/update.sh

.PHONY: server
server: ## is used to start the application
	@./scripts.d/server.sh

##@ Testing
.PHONY: test
test: setup ## is used to run the test suite of the application
	@./scripts.d/test.sh

.PHONY: cibuild
cibuild: ## is used for your continuous integration server
	@./scripts.d/cibuild.sh

.PHONY: clean
clean: ## is used to reset the infrastructure o an inditial state
	@echo "is used to reset the infrastructure o an inditial state"
