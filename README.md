# Symfony Demo

[![pipeline status](https://gitlab.com/wa-pipelines/samples/symfony/badges/main/pipeline.svg)](https://gitlab.com/wa-pipelines/samples/symfony/-/commits/main)
[![coverage report](https://gitlab.com/wa-pipelines/samples/symfony/badges/main/coverage.svg)](https://gitlab.com/wa-pipelines/samples/symfony/-/commits/main)
[![Latest Release](https://gitlab.com/wa-pipelines/samples/symfony/-/badges/release.svg)](https://gitlab.com/wa-pipelines/samples/symfony/-/releases)
