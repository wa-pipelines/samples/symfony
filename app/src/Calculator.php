<?php

/**
 * This file contains the Calculator class which provides basic math operations.
 *
 * PHP version 8.2
 *
 * @category Math
 * @package  App
 * @author   Your Name <your.email@example.com>
 * @license  http://opensource.org/licenses/MIT MIT License
 * @link     http://example.com
 */

namespace App;

/**
 * Class Calculator
 *
 * Provides basic mathematical operations.
 *
 * @category Math
 * @package  App
 * @author   Your Name <your.email@example.com>
 * @license  http://opensource.org/licenses/MIT MIT License
 * @link     http://example.com
 */
class Calculator
{
    /**
     * Returns the identity of a number.
     *
     * @param int|float $foo The input number.
     *
     * @return int|float The identity of the input number.
     *
     * @throws \InvalidArgumentException If either $foo is not an integer or a float.
     */
    public function identity($foo): int|float
    {
        if (!is_int($foo) && !is_float($foo)) {
            throw new \InvalidArgumentException('Input must be an integer or a float.');
        }

        return +$foo;
    }

    /**
     * Returns the negation of a number.
     *
     * @param int|float $foo The input number.
     *
     * @return int|float The negation of the input number.
     *
     * @throws \InvalidArgumentException If either $foo is not an integer or a float.
     */
    public function negation($foo): int|float
    {
        if (!is_int($foo) && !is_float($foo)) {
            throw new \InvalidArgumentException('Input must be an integer or a float.');
        }
        return -$foo;
    }

    /**
     * Adds two numbers.
     *
     * @param int|float $foo The first number.
     * @param int|float $bar The second number.
     *
     * @return int|float The sum of the two numbers.
     *
     * @throws \InvalidArgumentException If either $foo or $bar is not an integer or a float.
     */
    public function addition($foo, $bar): int|float
    {
        if ((!is_int($foo) && !is_float($foo)) || (!is_int($bar) && !is_float($bar))) {
            throw new \InvalidArgumentException('Both arguments must be integers or floats.');
        }
        return $foo + $bar;
    }

    /**
     * Subtracts the second number from the first number.
     *
     * @param int|float $foo The first number.
     * @param int|float $bar The second number.
     *
     * @return int|float The difference of the two numbers.
     *
     * @throws \InvalidArgumentException If either $foo or $bar is not an integer or a float.
     */
    public function subtraction($foo, $bar): int|float
    {
        if ((!is_int($foo) && !is_float($foo)) || (!is_int($bar) && !is_float($bar))) {
            throw new \InvalidArgumentException('Both arguments must be integers or floats.');
        }
        return $foo - $bar;
    }

    /**
     * Multiplies two numbers.
     *
     * @param int|float $foo The first number.
     * @param int|float $bar The second number.
     *
     * @return int|float The product of the two numbers.
     *
     * @throws \InvalidArgumentException If either $foo or $bar is not an integer or a float.
     */
    public function multiplication($foo, $bar): int|float
    {
        if ((!is_int($foo) && !is_float($foo)) || (!is_int($bar) && !is_float($bar))) {
            throw new \InvalidArgumentException('Both arguments must be integers or floats.');
        }
        return $foo * $bar;
    }

    /**
     * Divides the first number by the second number.
     *
     * @param int|float $foo The first number.
     * @param int|float $bar The second number.
     *
     * @return int|float The quotient of the division.
     *
     * @throws \InvalidArgumentException If either $foo or $bar is not an integer or a float.
     */
    public function division($foo, $bar): int|float
    {
        if ($bar === 0) {
            throw new \InvalidArgumentException('Division by zero is forbiden');
        }

        if ((!is_int($foo) && !is_float($foo)) || (!is_int($bar) && !is_float($bar))) {
            throw new \InvalidArgumentException('Both arguments must be integers or floats.');
        }

        return $foo / $bar;
    }

    /**
     * Returns the remainder of the division of the first by the second number.
     *
     * @param int|float $foo The first number.
     * @param int|float $bar The second number.
     *
     * @return int|float The remainder of the division.
     *
     * @throws \InvalidArgumentException If either $foo or $bar is not an integer or a float.
     */
    public function modulo($foo, $bar): int|float
    {
        if ($bar === 0) {
            throw new \InvalidArgumentException('Modulo by zero is forbidden.');
        }

        if ((!is_int($foo) && !is_float($foo)) || (!is_int($bar) && !is_float($bar))) {
            throw new \InvalidArgumentException('Both arguments must be integers or floats.');
        }

        if (is_float($foo) || is_float($bar)) {
            return fmod($foo, $bar);
        }

        return $foo % $bar;
    }

    /**
     * Raises the first number to the power of the second number.
     *
     * @param int|float $foo The base number.
     * @param int       $bar The exponent.
     *
     * @return int|float The result of the exponentiation.
     *
     * @throws \InvalidArgumentException If either $foo or $bar is not an integer or a float.
     */
    public function exponentiation($foo, $bar): int|float
    {
        return $foo ** $bar;
    }
}
