<?php

/**
 * This file contains the CalculatorController class which handles API endpoints
 * for calculator operations.
 *
 * PHP version 8.2
 *
 * @category Controller
 * @package  App\Controller
 * @author   Your Name <your.email@example.com>
 * @license  http://opensource.org/licenses/MIT MIT License
 * @link     http://example.com
 */

namespace App\Controller;

use App\Calculator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CalculatorController
 *
 * This class handles API endpoints for calculator operations.
 *
 * @category Controller
 * @package  App\Controller
 * @author   Your Name <your.email@example.com>
 * @license  http://opensource.org/licenses/MIT MIT License
 * @link     http://example.com
 */
class CalculatorController extends AbstractController
{
    /**
     * Index route for the CalculatorController.
     *
     * @return JsonResponse The welcome message.
     *
     * @Route("/calculator", name="app_calculator")
     */
    #[Route('/calculator', name: 'app_calculator')]
    public function index(): JsonResponse
    {
        return $this->json(
            [
                'message' => 'Welcome to your new controller!',
                'path' => 'src/Controller/CalculatorController.php',
            ]
        );
    }

    /**
     * Addition API endpoint.
     *
     * @param Request    $request    The HTTP request object.
     * @param Calculator $calculator The Calculator service.
     *
     * @return JsonResponse The result of the addition.
     *
     * @Route("/api/addition", name="calculator_addition")
     */
    #[Route('/api/addition', name: 'calculator_addition')]
    public function addition(Request $request, Calculator $calculator): JsonResponse
    {
        $foo = $request->request->get('foo');
        $bar = $request->request->get('bar');
        $result = $calculator->addition($foo, $bar);

        return new JsonResponse(
            [
                'result' => $result
            ]
        );
    }
}
