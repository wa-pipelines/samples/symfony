<?php

/**
 * This file is part of the Symfony application.
 *
 * PHP version 8.2
 *
 * @category Framework
 * @package  App
 * @author   Your Name <your.email@example.com>
 * @license  http://opensource.org/licenses/MIT MIT License
 * @link     http://example.com
 */

namespace App;

use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;

/**
 * Class Kernel
 *
 * This class is the main entry point for the Symfony application.
 *
 * @category Framework
 * @package  App
 * @author   Your Name <your.email@example.com>
 * @license  http://opensource.org/licenses/MIT MIT License
 * @link     http://example.com
 */
class Kernel extends BaseKernel
{
    use MicroKernelTrait;
}
