<?php

// tests/Unit/CalculatorTest.php
namespace App\Tests\Unit;

use App\Calculator;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{
    private Calculator $calculator;

    protected function setUp(): void
    {
        parent::setUp();
        $this->calculator = new Calculator();
    }

    public function testIdentity(): void {
        $this->assertSame(5, $this->calculator->identity(5));
        $this->assertSame(-5, $this->calculator->identity(-5));
        $this->assertSame(5.5, $this->calculator->identity(5.5));
        $this->assertSame(-5.5, $this->calculator->identity(-5.5));
        $this->assertSame(0, $this->calculator->identity(0));
    }

    public function testNegation(): void {
        $this->assertSame(-5, $this->calculator->negation(5));
        $this->assertSame(5, $this->calculator->negation(-5));
        $this->assertSame(-5.5, $this->calculator->negation(5.5));
        $this->assertSame(5.5, $this->calculator->negation(-5.5));
        $this->assertSame(0, $this->calculator->negation(0));
    }

    public function testAdition(): void {
        $this->assertSame(0, $this->calculator->addition(0, 0));
        $this->assertSame(0, $this->calculator->addition(1, -1));
        $this->assertSame(1, $this->calculator->addition(2, -1));
        $this->assertSame(-1, $this->calculator->addition(1, -2));
        $this->assertSame(-1, $this->calculator->addition(-1, 0));
        $this->assertSame(1.0, $this->calculator->addition(0.5, 0.5));
        $this->assertSame(0.0, $this->calculator->addition(0.5, -0.5));
        $this->assertSame(-0.5, $this->calculator->addition(0, -0.5));
    }

    public function testSubstraction(): void {
        $this->assertSame(0, $this->calculator->subtraction(0, 0));
        $this->assertSame(2, $this->calculator->subtraction(1, -1));
        $this->assertSame(3, $this->calculator->subtraction(2, -1));
        $this->assertSame(3, $this->calculator->subtraction(1, -2));
        $this->assertSame(-1, $this->calculator->subtraction(-1, 0));
        $this->assertSame(0.0, $this->calculator->subtraction(0.5, 0.5));
        $this->assertSame(1.0, $this->calculator->subtraction(0.5, -0.5));
        $this->assertSame(0.5, $this->calculator->subtraction(0, -0.5));
    }

    public function testMultiplication(): void {
        $this->assertSame(0, $this->calculator->multiplication(0, 0));
        $this->assertSame(-1, $this->calculator->multiplication(1, -1));
        $this->assertSame(-2, $this->calculator->multiplication(2, -1));
        $this->assertSame(-2, $this->calculator->multiplication(1, -2));
        $this->assertSame(0, $this->calculator->multiplication(-1, 0));
        $this->assertSame(0.25, $this->calculator->multiplication(0.5, 0.5));
        $this->assertSame(-0.25, $this->calculator->multiplication(0.5, -0.5));
        $this->assertSame(0.0, $this->calculator->multiplication(0, -0.5));
    }

    public function testDivision(): void {
        $this->assertSame(-1, $this->calculator->division(1, -1));
        $this->assertSame(-2, $this->calculator->division(2, -1));
        $this->assertSame(-0.5, $this->calculator->division(1, -2));
        $this->assertSame(1.0, $this->calculator->division(0.5, 0.5));
        $this->assertSame(-1.0, $this->calculator->division(0.5, -0.5));
        $this->assertSame(0.0, $this->calculator->division(0, -0.5));
    }

    public function testModulo(): void {
        $this->assertSame(0, $this->calculator->modulo(1, -1));
        $this->assertSame(0, $this->calculator->modulo(2, -1));
        $this->assertSame(1, $this->calculator->modulo(1, -2));
        $this->assertSame(0, $this->calculator->modulo(0, -2));
        $this->assertSame(0, $this->calculator->modulo(0, 2));
        $this->assertSame(0.0, $this->calculator->modulo(0.5, 0.5));
        $this->assertSame(0.4, $this->calculator->modulo(1, 0.6));
        $this->assertSame(0.0, $this->calculator->modulo(0, -0.5));
    }

    public function testExponentiation(): void {
        $this->assertSame(1, $this->calculator->exponentiation(0, 0));
        $this->assertSame(1.0, $this->calculator->exponentiation(1, -1));
        $this->assertSame(0.5, $this->calculator->exponentiation(2, -1));
        $this->assertSame(1.0, $this->calculator->exponentiation(1, -2));
        $this->assertSame(1, $this->calculator->exponentiation(-1, 0));
        $this->assertSame(2, $this->calculator->exponentiation(2, 1));
    }
}
