#!/usr/bin/env bash

cd app || exit 0

if [ ! -d vendor ]; then
  composer install
fi
